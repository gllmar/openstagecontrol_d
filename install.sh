#!/bin/bash

OSCV=1.22.0

getNode()
{
  if which node > /dev/null
    then
    	node -v
        echo "getNode : node is installed, skipping install"
    else
    	echo "getNode : node is not installed, getting current release"
    	curl -fsSL https://deb.nodesource.com/setup_current.x | sudo -E bash -
    
    	sudo apt install nodejs
    
    	node -v

    	sudo apt install build-essential
        # add deb.nodesource repo commands 
        # install node
    fi
    
	
}

getOpenStageControl()
{
	LINK=https://github.com/jean-emmanuel/open-stage-control/releases/download/v"$OSCV"/open-stage-control_"$OSCV"_node.zip
	echo "get $LINK"
	wget $LINK
	unzip open-stage-control_"$OSCV"_node.zip 
	rm open-stage-control_"$OSCV"_node.zip
	mv open-stage-control_"$OSCV"_node open-stage-control_node
}

getNode
getOpenStageControl 
