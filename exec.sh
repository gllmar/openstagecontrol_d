#!/bin/bash

SEND=127.0.0.1:10011
LOAD=~/src/pvlc/gui/test.json 
OSCPORT=10010
WEBPORT=8080


echo "	http://$HOSTNAME.local:$WEBPORT"

node open-stage-control_node/index.js -s $SEND -l $LOAD -o $OSCPORT -p $WEBPORT --no-qrcode


